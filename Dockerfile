FROM python:3.8.5-alpine
WORKDIR /app
ADD . /app

RUN pip3 install -r requirements.txt
CMD ["python3","main.py"]