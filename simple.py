from fastapi import APIRouter
from fastapi.templating import Jinja2Templates
from starlette.requests import Request

router=APIRouter()

templates=Jinja2Templates("")
@router.get("/")
async def home():
    return "<h1 style='color:blue'> This is home. </h1>"

@router.get("/temp")
async def get_temp(request:Request):
    return templates.TemplateResponse("sample.html", {"request": request,"mesage":"Rashmi"})