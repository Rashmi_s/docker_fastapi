import uvicorn
from fastapi import FastAPI, APIRouter
import users,simple

api_router = APIRouter()
app = FastAPI()

api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(simple.router, prefix="/simple", tags=["simple"])
app.include_router(api_router)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8090, debug=True)
